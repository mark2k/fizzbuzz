﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using FizzBuzzWeb;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace FizzBuzzWeb_Tests
{
	public class FizzBuzzIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
	{
		private readonly WebApplicationFactory<Startup> factory;

		public FizzBuzzIntegrationTests(WebApplicationFactory<Startup> factory)
		{
			this.factory = factory;
		}

		[Fact]
		public async Task Default_Endpoint_Returns_100_Items()
		{
			HttpClient client = factory.CreateClient();

			HttpResponseMessage response = await client.GetAsync("fizzbuzz/default").ConfigureAwait(false);
			JsonElement result = JsonDocument.Parse(await response.Content.ReadAsStringAsync()).RootElement;

			response.EnsureSuccessStatusCode();
			Assert.Equal(100, result.GetArrayLength());
		}

		[Fact]
		public async Task Can_Supply_Just_Max_Number_To_Custom_Endpoint()
		{
			HttpResponseMessage response = await GetResponse(@"{""MaxNumber"": 50}");
			var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
			JsonElement result = JsonDocument.Parse(json).RootElement;

			_ = response.EnsureSuccessStatusCode();
			Assert.Equal(50, result.GetArrayLength());
		}

		[Fact]
		public async Task Can_Supply_Just_Rules_To_Custom_Endpoint()
		{
			HttpResponseMessage response = await GetResponse(@"{""Rules"":[{""Divisor"":1,""Substitute"":""Fizz""}]}");
			var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
			JsonElement.ArrayEnumerator enumerator = JsonDocument.Parse(json).RootElement.EnumerateArray();
			_ = enumerator.MoveNext();

			_ = response.EnsureSuccessStatusCode();
			Assert.Equal("Fizz", enumerator.Current.ToString());
		}

		[Theory]
		[InlineData(@"{""Rules"":[{""Divvsor"":1,""Substitute"":""Fizz""}]}")]
		[InlineData(@"{""Rules"":[{""Divisor"":1,""Substitute"":null}]}")]
		[InlineData(@"{""Rules"":[{""Divisor"":1,""Substitute"":nul")]
		[InlineData(@"{""Rules"":[{""Divisor"":0,""Substitute"":""Fizz""}]}")]
		public async Task Badly_Formed_Rules_Returns_Bad_Request(string requestJson)
		{
			HttpResponseMessage response = await GetResponse(requestJson);
			_ = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

			Assert.Equal("BadRequest", response.StatusCode.ToString());
		}

		//TODO: More tests, verify results match rules, null rules return default enumeration etc.

		private async Task<HttpResponseMessage> GetResponse(string json)
		{
			HttpClient client = factory.CreateClient();
			var request = new HttpRequestMessage
			{
				Method = HttpMethod.Get,
				RequestUri = new Uri($"{factory.Server.BaseAddress}fizzbuzz/custom"),
				Content = new StringContent(json, Encoding.UTF8, "application/json")
			};

			return await client.SendAsync(request).ConfigureAwait(false);
		}
	}
}
