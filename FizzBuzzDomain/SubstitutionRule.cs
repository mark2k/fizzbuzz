﻿using System;

namespace FizzBuzzDomain
{
	public class SubstitutionRule
	{
		public ushort Divisor { get; }
		public string Substitute { get; }

		public SubstitutionRule(ushort divisor, string substitute)
		{
			Divisor = divisor > 0 ? divisor : throw new ArgumentOutOfRangeException(nameof(divisor), "Divisor in rule cannot be zero.");
			Substitute = substitute ?? throw new ArgumentNullException(nameof(substitute), "Substituted string cannot be null.");
		}
	}
}