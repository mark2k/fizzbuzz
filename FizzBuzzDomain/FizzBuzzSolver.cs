﻿using System.Collections.Generic;
using System.Text;

namespace FizzBuzzDomain
{
	public class FizzBuzzSolver
	{
		public const int DefaultMaxNumber = 100;
		private readonly ushort maxNumber;
		private readonly IList<SubstitutionRule> rules = new List<SubstitutionRule>
													{
														new SubstitutionRule(3, "Fizz"),
														new SubstitutionRule(5, "Buzz")
													};

		public FizzBuzzSolver(ushort maxNumber = DefaultMaxNumber)
		{
			this.maxNumber = maxNumber;
		}

		public FizzBuzzSolver(IList<SubstitutionRule> rules, ushort maxNumber = DefaultMaxNumber)
		{
			this.maxNumber = maxNumber;
			if (rules != null)
				this.rules = rules;
		}

		public IList<string> Solve()
		{
			var result = new List<string>();

			for (var i = 1; i <= maxNumber; i++)
			{
				var itemRepresentation = new StringBuilder();
				var substituted = false;
				foreach (var r in rules)
				{
					if (i % r.Divisor == 0)
					{
						_ = itemRepresentation.Append(r.Substitute);
						substituted = true;
					}
				}
				if (!substituted)
					_ = itemRepresentation.Append(i);
				result.Add(itemRepresentation.ToString());
			}
			return result;
		}
	}
}