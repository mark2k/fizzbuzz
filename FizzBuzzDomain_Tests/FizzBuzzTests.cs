using System.Collections.Generic;
using FizzBuzzDomain;
using Xunit;

namespace FizzBuzzDomain_Tests
{
	public class FizzBuzzTests
	{
		[Theory]
		[InlineData(50)]
		[InlineData(ushort.MaxValue)]
		[InlineData(0)]
		[InlineData(1)]
		public void Given_Max_Number_N_When_Enumerated_Then_N_Items_Are_Returned(ushort maxNumber)
		{
			var sut = new FizzBuzzSolver(maxNumber);

			IList<string> result = sut.Solve();

			Assert.Equal(maxNumber, result.Count);
		}

		[Fact]
		public void Given_A_Substitue_And_A_Divisor_When_Number_Is_Evenly_Divisible_By_Divisor_Then_Item_Output_Is_Substitute()
		{
			var rules = new List<SubstitutionRule> { new SubstitutionRule(4, "Foo") };
			var sut = new FizzBuzzSolver(rules);

			IList<string> result = sut.Solve();

			Assert.Equal("Foo", result[15]);
		}

		[Theory]
		[InlineData(15, "FizzBuzz")]
		[InlineData(30, "FooFizzBuzz")]
		public void When_Number_Matches_Multiple_Rules_Then_Item_Output_Is_Concatenated_Substitutes(int number, string expected)
		{
			var sut = new FizzBuzzSolver(new List<SubstitutionRule>
			{
				new SubstitutionRule(10, "Foo"),
				new SubstitutionRule(3, "Fizz"),
				new SubstitutionRule(5, "Buzz")
			});

			IList<string> result = sut.Solve();

			Assert.Equal(expected, result[number - 1]);
		}

		[Fact]
		public void Given_No_Max_Number_When_Enumerated_100_Items_Are_Returned_()
		{
			var sut = new FizzBuzzSolver();

			IList<string> result = sut.Solve();

			Assert.Equal(100, result.Count);
		}

		[Theory]
		[InlineData(3, "Fizz")]
		[InlineData(5, "Buzz")]
		[InlineData(2, "2")]
		public void Given_No_Substitution_Rules_When_Enumerated_Default_Substitution_Rules_Are_Applied_(int number, string expected)
		{
			var sut = new FizzBuzzSolver();

			IList<string> result = sut.Solve();

			Assert.Equal(expected, result[number - 1]);
		}

		[Theory]
		[InlineData(3, "Fizz")]
		[InlineData(5, "Buzz")]
		[InlineData(2, "2")]
		public void Given_Null_Rules_When_Enumerated_Default_Substition_Rules_Are_Applied(int number, string expected)
		{
			var sut = new FizzBuzzSolver(null);
			IList<string> result = sut.Solve();

			Assert.Equal(expected, result[number - 1]);
		}

		[Theory]
		[InlineData(3, "Fizz")]
		[InlineData(5, "Buzz")]
		[InlineData(2, "2")]
		public void Given_Only_A_Max_Number_When_Enumerated_Default_Substition_Rules_Are_Applied(int number, string expected)
		{
			var sut = new FizzBuzzSolver(50);
			IList<string> result = sut.Solve();

			Assert.Equal(expected, result[number - 1]);
		}
	}
}

