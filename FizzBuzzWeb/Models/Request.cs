﻿using System.Collections.Generic;
using FizzBuzzDomain;

namespace FizzBuzzWeb.Models
{
	public class Request
	{
		public ushort? MaxNumber { get; set; }
		public List<RequestRule>? Rules { get; set; }
	}
}
