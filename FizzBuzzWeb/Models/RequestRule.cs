﻿namespace FizzBuzzWeb.Models
{
	public class RequestRule
	{
		public ushort Divisor { get; set; }
		public string? Substitute { get; set; }
	}
}
