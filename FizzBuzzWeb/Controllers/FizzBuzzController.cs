﻿using System;
using System.Collections.Generic;
using FizzBuzzDomain;
using FizzBuzzWeb.Models;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzWeb.Controllers
{
	[Route("fizzbuzz/")]
	[ApiController]
	public class FizzBuzzController : ControllerBase
	{
		[HttpGet("default")]
		public ActionResult<IEnumerable<string>> EnumerateWithDefaults()
		{
			var solver = new FizzBuzzSolver();
			return Ok(solver.Solve());
		}

		[HttpGet("custom")]
		public ActionResult<IEnumerable<string>> EnumerateWithCustomParameters(Request request)
		{
			FizzBuzzSolver solver;

			if (request.MaxNumber == null)
				request.MaxNumber = FizzBuzzSolver.DefaultMaxNumber;

			if (request.Rules == null)
			{
				solver = new FizzBuzzSolver((ushort)request.MaxNumber);
				return Ok(solver.Solve());
			}

			try
			{
				solver = new FizzBuzzSolver(ConvertRules(request.Rules), (ushort)request.MaxNumber);
			}
			catch (ArgumentNullException)
			{
				return BadRequest("Must supply a non-null, well-formed rules property and substitutes.");
			}
			catch (ArgumentOutOfRangeException)
			{
				return BadRequest("Must supply a positive divisor.");
			}

			return Ok(solver.Solve());
		}

		private static List<SubstitutionRule> ConvertRules(List<RequestRule> rules)
		{
			var convertedRules = new List<SubstitutionRule>();

			foreach (var r in rules)
				convertedRules.Add(new SubstitutionRule(r.Divisor, r.Substitute));
			return convertedRules;
		}
	}
}
